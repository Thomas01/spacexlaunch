/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

const documents = {
    "\n   query LaunchDates {\n    launchesPast(limit: 1) {\n        launch_date_local\n      }\n      launchNext {\n        launch_date_local\n      }\n  }\n": types.LaunchDatesDocument,
};

export function graphql(source: "\n   query LaunchDates {\n    launchesPast(limit: 1) {\n        launch_date_local\n      }\n      launchNext {\n        launch_date_local\n      }\n  }\n"): (typeof documents)["\n   query LaunchDates {\n    launchesPast(limit: 1) {\n        launch_date_local\n      }\n      launchNext {\n        launch_date_local\n      }\n  }\n"];

export function graphql(source: string): unknown;
export function graphql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;