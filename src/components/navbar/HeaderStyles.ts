import { FaBars } from 'react-icons/fa';
import styled from 'styled-components';
import { NavLink as Link } from "react-router-dom";
import {open} from '../../Types'

export const Navbar =  styled.nav<open>`
 display: flex;
 position: sticky;
 top: 0;
 justify-content: space-between ;
 width: 100%;
 height: ${(props) => (props.open ? "100vh" : "80px")};
 background-color: #000;
 z-index: 99 ;

 @media (min-width: 1000px) {
    height: 100px;
  }
`;

export const Bars = styled(FaBars)`
  display: none;
  color: #FFF;
  @media screen and (max-width: 1000px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`;

export const Logo = styled.img`
 cursor: pointer;
 width : 70%;
 height: 80px;
 color: #FFF;
 object-fit: cover;
 margin-left: 70px ;
 @media screen and (max-width: 1000px) {
  width : 70%;
  margin-left: 30px;
  height: 80px;
}
`;

export const NavLinks = styled.div`
  position: relative;
  left: 60%;
  top: 0;
  width: 40%;
  padding-top: 2rem ;

  @media screen and (max-width: 1000px) {
   display: none ;
  }
`;
export const ListItem = styled(Link)`
  padding: 10px;
  color: #FFF;
  cursor: pointer;
  font-size: 0.72rem;
  letter-spacing:.5px;
  font-family: 'Times New Roman', Times, serif ;
  font-weight: 500 ;
  text-align: center;
  text-decoration: none;
  &.active {
    color: #999;
  }
  &:hover {
    transition: all 0.2s ease-in-out;
    color: #2c9b3e;
    
  }
  @media screen and (max-width: 768px) {
   .izpgmz.active {
    width: 80%;
  }
  }
`;

export const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  margin-right: 24px;
  @media screen and (max-width: 1000px) {
    display: none;
  }
`;

export const NavBtnLink = styled(Link)`
  border-radius: 4px;
  background: #FFF;
  padding: 10px 22px;
  color: #FFF;
  outline: none;
  border: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;
  
  margin-left: 24px;
  &:hover {
    transition: all 0.2s ease-in-out;
    background: #fff;
    color: #2c9b3e;
  }
`;

export const LinkLogo = styled(Link)`
  position: absolute ;
  top:0 ;
  color: #FFF;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  width: 23%;
  height: 80px;
  @media screen and (max-width: 1000px) {
    width: 70%;
    height: 100px ;
  }
`;

export const Close = styled.button`
 background-color: #000 ;
`;

export const OpenMenuButton = styled.button`
  position: absolute;
  right: 0;  
  cursor: pointer;
  width: 70px;
  height: 50px;
  padding: 12px;
  text-align:center;
  background: none;
  border: none;
  color: white;
  font-size: 30px;
  @media (min-width: 1000px) {
    display: none;
    width:100%;
  }
`;

export const NavbarExtendedContainer = styled.div`
  position: absolute;
  top: 25%;
  padding:12px ;
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (min-width: 1000px) {
    display: none;
  }
`;

export const NavbarLinkExtended = styled(Link)`
  color: #FFF;
  font-size: 1.5rem;
  text-decoration: none;
  margin: 10px;
  text-align:center ;
  letter-spacing:1.5px;
  font-family: 'Times New Roman', Times, serif ;
  font-weight: 500 ;
`;