import React, {useState} from 'react';
import LogoImage from '../../assests/SpaceXLogo.png'
import { Navbar, Logo, LinkLogo, NavbarLinkExtended, ListItem, NavLinks, Bars, NavbarExtendedContainer, OpenMenuButton } from './HeaderStyles';

const Header = () => {
  const [open, setOpen] = useState(false);
    return (
       <Navbar open={open}>
          <LinkLogo to="/">
          <Logo src={LogoImage} alt='' />
         </LinkLogo>
        <NavLinks>
          <ListItem to="/">HOME</ListItem>
          <ListItem to="/last-launched">LAST ROCKET LAUNCHED</ListItem>
          <ListItem to="/next-launch">NEXT ROCKET LAUNCH</ListItem>
        </NavLinks>
         <OpenMenuButton onClick={() => {setOpen((curr) => !curr);}}>
         {open ? <>&#10005;</> : <><Bars /></>}
         </OpenMenuButton>
         {open && (
          <NavbarExtendedContainer>
            <NavbarLinkExtended to="/">HOME</NavbarLinkExtended>
            <NavbarLinkExtended to="/last-launched">LAST ROCKET LAUNCHED</NavbarLinkExtended>
            <NavbarLinkExtended to="/next-launch">NEXT ROCKET LAUNCH</NavbarLinkExtended>
          </NavbarExtendedContainer>
         )}
       </Navbar>
    );
}

export default Header;