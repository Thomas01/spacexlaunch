import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { GlobalStyles } from './Globalstyles';
import Header from './components/navbar/Header';

//pages
import Home from './pages/Home';
import LastLaunched from './pages/LastLaunched';
import NextLaunched from './pages/NextLaunched';

function App() {
  return (
    <>
      <GlobalStyles />
      
    <BrowserRouter>
    <Header />
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/last-launched" element={<LastLaunched /> } />
      <Route path="/next-launch" element={<NextLaunched /> } />
    </Routes>
  </BrowserRouter>
  </>
    
  );
}

export default App;
