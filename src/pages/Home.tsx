import React,{useState, useEffect} from 'react';
import { HomeWrapper,
         PageWrapper 
        ,DateWrapper,
         DateButton,
         Section,
         DateButtonRight,
         Heading } from './PageStyles';
import { useQuery } from '@apollo/client';
import { QUERY_LAUNCH_DATES } from '../graphql/query';
import { LaunchDatesQuery } from '../generated/schema/graphql'

const Home = () => {
  
  const { loading, error, data } = useQuery(QUERY_LAUNCH_DATES)
       const[dateNext, setDateNext] = useState("")
       const[datePast, setDatePast] = useState("")

       useEffect(() => {
        hundleNextLaunch()
        hundlePastLaunched()
       },)

       const hundleNextLaunch = () =>{
        if(loading){
          setDateNext("Loading!")
        } else {
         let unixTimestamp = data.launchNext.launch_date_unix
         return setDateNext(formatDate(unixTimestamp))
        }
       }

      const hundlePastLaunched = () =>{
        if(loading){
          setDatePast("Loading!")
        } else {
         let {launch_date_unix} = data.launchesPast.find((date: LaunchDatesQuery) => date.launch_date_unix === 1604618640)
         return setDatePast(formatDate(launch_date_unix))
        }
      } 

        //Date fomatter 
       const formatDate = (date : number) =>{
        let formatedTime
        const time = new Date(date * 1000)
        return formatedTime = time.toLocaleDateString("default")
       }
       
    return (
        <PageWrapper>
           <HomeWrapper>
              {error && (<Heading>Oops! Something went wrong.
               Please try again or check your network connection.
               </Heading> )}

              {!loading && !error ? <Heading> SpaceX designs, manufactures and launches advanced rockets and spacecraft. The company was founded in 2002 to revolutionize
               space technology, with the ultimate goal of enabling people to live on other planets. <Section>Below
               are the dates for last launched and next launch date</Section></Heading> : ""} 
               
               
                

             <DateWrapper>
               <DateButton>{dateNext}</DateButton>
               <DateButtonRight>{datePast}</DateButtonRight>
             </DateWrapper>
           </HomeWrapper>
        </PageWrapper> 
    );
}
export default Home;