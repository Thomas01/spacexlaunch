import React,{useState, useEffect} from 'react';
import { PageWrapper, NexPageWrapper, Paragraph } from "./PageStyles";
import { useQuery } from '@apollo/client';
import { QUERY_LAUNCH_DATES } from '../graphql/query';

const NextLaunched = () => {
    const { loading, error, data } = useQuery(QUERY_LAUNCH_DATES)
    const[nextDate, setNextDate] = useState("")

    useEffect(() => {
    dateFormat()
    },)

       const dateFormat = () =>{
        if(loading){
            setNextDate("Loading")
        } else {
         let information = data.launchNext.details
         setNextDate(information)
        }
       }
    return (
        <PageWrapper>
            <NexPageWrapper>
              {error && (<Paragraph>Oops! Something went wrong.
               Please try again or check your network connection.
               </Paragraph> )}

              <Paragraph>
               { nextDate}
              </Paragraph>
            </NexPageWrapper>
        </PageWrapper>
    )
}
export default NextLaunched;