import styled from 'styled-components';
//images
import cover from '../assests/Starlink-2-Falcon-9.jpg';
import NextPageImages from '../assests/rocket-launch.webp';
import HomeImage from '../assests/spacex-starlink.gif';

export const PageWrapper = styled.div`
 width: 100%;
 background-color: #000 ;
`;

export const HomeWrapper = styled.div`
 background-image: url(${HomeImage});
 background-position: center center;
 background-repeat: no-repeat;
 background-size: cover;
 height: 100vh;
`;
export const Heading = styled.p`
 font-size: 18px;
 text-align: center; 
 line-height: 30px;
 padding-top: 8.5rem;
 font-weight: 400;
 width:50% ;
 color: #FFF;
 margin: auto;

 @media screen and (max-width: 768px) {
  width : 92%;
  padding: 12px;
  font-size: 1.16rem;
}
`;

//Date button
export const DateWrapper = styled.div`
 position: relative;
 top: 7%;
 display: flex;
 justify-content: space-between;
 width: 65%;
 margin: auto;
 @media screen and (max-width: 768px) {
  display:flex;
  flex-direction: column ;
  align-items: center;
  width : 90%;
  margin: auto;
}
`;

export const DateButton = styled.button`
 display: block;
 width: 12rem;
 padding: 0.8rem;
 font-weight: 500;
 font-size: .9rem;
 letter-spacing: 1.2px;
 color: #2c9b3e;
 background: transparent;
 border: 2px solid #2c9b3e;
 border-radius: 6px;
 text-transform: uppercase;
 outline: 0;
 overflow: hidden;
 cursor: pointer;
 @media screen and (max-width: 768px) {
    margin-top: 14px;
    width: 20rem;
    padding: .9rem;
    font-size: 1rem;
    font-weight: 700
}
`;

export const DateButtonRight = styled(DateButton)``;

// Last Launched Pages
export const LastPageWrapper = styled.div`
 width : 100%;
 height: 90vh;
 background-color: #000;
`;

export const LeftSidePage = styled.div`
 width: 40%;
 height: 89vh;
 position: fixed;
 top: 13%;
 background-image: url(${cover});
 background-position: center center;
 background-repeat: no-repeat;
 background-size: cover;
 @media screen and (max-width: 768px) {
  display: none; 
}
`;

export const Section = styled.p`
 color: #999;
`;

export const RightSidePage = styled.div`
 width: 60%;
 float: right;
 height: 90vh;
 @media screen and (max-width: 768px) {
  width : 100%; 
  background-image: url(${cover});
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
}
`;
export const Paragraphy = styled.div`
 position: relative;
 text-align: center;
 top: 22%;
 line-height: 40px;
 font-size: 17px ;
 padding: 10px 60px 5px 2px;
 color: #FFF;
 @media screen and (max-width: 768px) {
    font-size: 1.4rem;
    padding: 10px;
    top: 10%;
    font-weight: 600;
}
`;

// Next Page
export const NexPageWrapper = styled(HomeWrapper)`
 background-image: url(${NextPageImages});
 background-attachment: fixed;
`;

export const Paragraph = styled(Paragraphy)`
 color: #FFF;
 top: 17%;
 margin: auto;
 font-size: 20px;
 letter-spacing: 3px;
 font-weight: 900;
 font-family:'Times New Roman', Times, serif ;
 width: 70%;
 @media screen and (max-width: 1200px) {
    width: 100%;
    font-size: 1.4rem;
    padding: 10px;
    top: 10%;
    color: #FFF;
    font-weight: 900;
}
`;