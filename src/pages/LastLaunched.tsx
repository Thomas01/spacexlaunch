import React,{useState, useEffect} from 'react';
import { PageWrapper, LastPageWrapper, LeftSidePage, RightSidePage, Paragraphy } from "./PageStyles";
import { useQuery } from '@apollo/client';
import { QUERY_LAUNCH_DATES } from '../graphql/query';
import { LaunchDatesQuery } from '../generated/schema/graphql';

const LastLaunched = () => {
    const { loading, error, data } = useQuery(QUERY_LAUNCH_DATES);
    const [pastDate, setPastDate] = useState("")

    useEffect(() => {
    hunledPastLaunch()
    },)

    const hunledPastLaunch = () =>{
        if(loading){
            setPastDate("Loading")
        } else {
         let { details } = data.launchesPast.find((date: LaunchDatesQuery) => date.launch_date_unix === 1604618640)
         setPastDate(details)
        }
       } 
    return (
        <PageWrapper>
            <LastPageWrapper>
            {error && (<Paragraphy>Oops! Something went wrong.
               Please try again or check your network connection.
               </Paragraphy> )}
              <LeftSidePage />
                <RightSidePage>
                    <Paragraphy>{pastDate}.</Paragraphy>
                </RightSidePage>
            </LastPageWrapper>
        </PageWrapper>
    );
}
export default LastLaunched;