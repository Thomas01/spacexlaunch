import { gql } from '@apollo/client';

export const QUERY_LAUNCH_DATES = gql`
   query LaunchDates {
    launchesPast(limit: 10) {
      launch_date_unix
      details
      }
      launchNext {
      launch_date_unix
      details
      }
  }
`;
